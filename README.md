# Projet de jeu de carte "Timeline Informatique"

> Un jeu de cartes open-source sans utilisation commerciale sur la chronologie
> de l'informatique, à imprimer et à jouer.

Voir en ligne: https://terra-numerica.frama.io/jeux/timeline/

## Historique

Réalisé initialement pour la fête de la science 2017 au département informatique
de l'Université Claude Bernard Lyon 1 donc voici le
[dépôt](https://github.com/romulusFR/timeline_informatique).

En 2024, il a été modifié dans le cadre d'un stage à
[TerraNumerica](https://terra-numerica.org/).

## Présentation

Le jeu est généré en PDF en utilisant LaTeX à partir de la description des
cartes dans un fichier CSV. Le processus de génération **est entièrement
automatisé**, ce qui inclut :

- le téléchargement des images;
- le renommage "de courtoisie" des images téléchargées;
- le recentrage automatique des images;
- la génération des fichiers LaTeX individuels (un verso, un recto);
- la génération des planches LaTeX (soit 1 soit 9 carte par page) avec padding
  de cartes blanches si le nombre de cartes n'est pas multiple de 9;

La génération de carte dans un autre format que les cartes poker (par défaut)
n'est _pas complètement automatisée_, elle nécessite de modifier les bases de
cartes LaTeX.

### Dépendences

- Python avec gestion des dépendances avec pipenv;
- la génération des `.pdf` nécessite une chaine `lualatex` avec TikZ;

### Installation et execution

```bash
git clone https://github.com/romulusFR/timeline_informatique.git

# installer les dépendences
pipenv sync

pipenv run python build.py  ./content/timeline.csv

# create escape game with ascii codes
pipenv run python build.py --type escape ./content/timeline.csv

# generate pdf avec lualatex (ou xelatex)
lualatex --output-directory=www/public  cartes_timeline.tex 
```

## Licences

Le projet provenant de diverses sources les licences sont multiples.

### Code python

Le code Python est sous licence MIT, par Benjamin Abel voir [./LICENCE-CODE](LICENCE-CODE).

### Licence du projet initial

Le contenu présent projet est en licence Creative Commons BY-NC-SA 3.0, par Romuald THION

Vous devez créditer l'oeuvre, vous n'êtes pas autorisé à faire un usage
commercial de cette oeuvre et de tout ou partie du matériel la composant. Dans
le cas où vous effectuez un remix, que vous transformez, ou créez à partir du
matériel composant l'oeuvre originale, vous devez diffuser l'oeuvre modifiée
dans les même conditions.

<https://creativecommons.org/licenses/by-nc-sa/3.0/fr/>

### Modèle de carte LaTeX

Le modèle de carte LaTeX est adapté de celui de Arvid sous licence CC-BY en tant
que contenu de stackoverflow. Il utilise TikZ, voir les fichiers
[./latex/tikzcards.tex](./latex/tikzcards.tex) et
[./latex/packages.tex](./latex/packages.tex)

- <https://tex.stackexchange.com/questions/47924/creating-playing-cards-using-tikz>
- <https://tex.stackexchange.com/questions/243740/print-double-sided-playing-cards>

### Licences des contenus

- les contenus textuels des cartes sont
  [sous licence CC BY-SA 3.0 de wikipedia](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Citation_et_r%C3%A9utilisation_du_contenu_de_Wikip%C3%A9dia)
- les images des cartes sont créditées individuellement (domaine public, CHM
  Timeline, Wikipedia etc.) dans les définitions des cartes

### Polices

Les polices INRIA sont sous licence [Open Font License](https://openfontlicense.org/).

## Contenu et description des cartes

Les cartes sont créées à partir d'un fichier `csv` au format `id,type,title,year,picture,credits,description`.

- _id_ : le numéro de la carte, n'apparait pas sur la carte
- _type_ : son type à choisir dans _pop, hard, soft, lang, theory, game, web, univ_ (apparait verticalement sur la gauche de la carte)
- _title_ : le nom de l'invention, de l'événement, de la personne ...
- _year_ : l'année de découverte (apparait avec le texte de la description au verso de la carte)
- _picture_ : une url vers une image dans un format supporté par pdfLaTeX
- _credits_ : crédits de l'illustration (apparait au recto de la carte uniquement)
- _description_ : court texte (apparait au verso de la carte)

### Cartes spéciales

La carte des règles est gérée séparément, elle est décrite (en dur) dans le dépôt (fichiers [`./special_cards/0_rules_front.tex`](./special_cards/0_rules_front.tex) et [`./special_cards/0_rules_back.tex`](./special_cards/0_rules_back.tex)).
Des cartes "blanches", sur le modèle [`./special_cards/00_blank.tex`](./special_cards/00_blank.tex) sont aussi automatiquement ajoutée au jeu si celui-ci n'est pas un multiple de 9 cartes.

### `timeline-full.csv`

Ce fichier contient l'ensemble des cartes du jeu créees intialement à partir de la frise du [computer history museum](https://www.computerhistory.org/timeline/).

Pas d'utilisation commerciale d'après la [licence](https://computerhistory.org/terms/) du site.

### `timeline.csv`

Fichier réduit à 9 dates par thème.

Les contenus sont issus de CHM, Wikipédia ou ont été écrits.

## Credits

Merci à l'association [Framasoft](https://framasoft.org/fr/) pour l'hébergement du code et de la page web.
