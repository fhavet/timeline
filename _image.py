import os
import io
import requests
from PIL import Image
import numpy as np
import tempfile

name = "Timeline cards tex generator"
homepage = "https://github.com/benabel/timeline_informatique"

# Convert these constants to uppercase
golden_height = 980
golden_width = 680
golden_ratio = golden_height / golden_width
strip_width_percent = 0.1666
ratio_threshold = 0.02


# Download an image from an uri and save it to a file as jpeg
def download(uri, filename):
    print("Downloading",uri, "to", filename)
    agent = f"{name} ({homepage}) python-requests/{requests.__version__}"
    headers = {"User-Agent": agent}
    response = requests.get(uri, headers=headers, stream=True)
    # write bytes to disk
    buffer = tempfile.SpooledTemporaryFile(max_size=1e9)
    response.raise_for_status()

    for chunk in response.iter_content(chunk_size=8192):
        buffer.write(chunk)

    buffer.seek(0)
    im = Image.open(io.BytesIO(buffer.read()))
    buffer.close()
    # Save to jpeg
    channels = im.split()
    if len(channels)>3:
        # convert RGBA to RGB with white background
        background = Image.new("RGB", im.size, color=(255, 255, 255))
        background.paste(im, mask=channels[3])
        background.save(filename, "JPEG", quality=95)
    else:
        im.convert('RGB').save(filename, "JPEG", quality=95)


def calculate_pixels_mean(fname):
    """Calculate pixel average value of the upper right part of image where the credits is writed"""
    im = Image.open(fname).convert("L")
    im = np.array(im)
    # crop upper right part of image
    im_crop = im[95 * im.shape[0] // 100 :, : 3 * im.shape[1] // 4]
    mean = im_crop.mean()
    return mean


# Resize images according to their format wrt the ideal one
# and calculate best color for credits
def resizer(input, output):
    if not os.path.exists(output):
        print(f"Resizing {input} to {output}")
        image = Image.open(input)
        width, height = image.size
        ratio = height / width
        target = {
            "width": width,
            "height": height,
            "left": 0,
            "top": 0,
            "flag": "normal",
        }

        if ratio > golden_ratio * (1 + ratio_threshold):
            target["height"] = round(width * golden_ratio)
            target["top"] = round((height - width * golden_ratio) / 2)
            target["flag"] = "vertical"
        elif ratio < golden_ratio * (1 - ratio_threshold):
            target["width"] = round(height / golden_ratio)
            target["left"] = (width - height / golden_ratio) / 2
            strip_width = (strip_width_percent * height) / golden_ratio
            delta = min(strip_width, target["left"])
            target["left"] = round(target["left"] - delta)
            target["flag"] = "horizontal"

        image = image.crop(
            (
                target["left"],
                target["top"],
                target["width"] + target["left"],
                target["height"] + target["top"],
            )
        )
        image = image.resize((golden_width, golden_height))
        image.save(output, "JPEG", quality=95)

    # TODO maybe could persist this value
    mean = calculate_pixels_mean(output)
    if mean < 128:
        return "white"
    return "black"
