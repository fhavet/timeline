import re
from slugify import slugify

HTML_FILE = "www/timeline.html"
# Récupère les types dans le fichier tex
TYPE_PATTERN = re.compile(r"\\newcommand{\\cardtype\w+}{\\cardtype{(\w+)bg}{{(.*)}}}")

theme_dict = {}
for line in open("latex/tikzcards.tex"):
    match = TYPE_PATTERN.match(line)
    if match:
        theme_dict[match[1]] = match[2]

HEAD = """<!doctype html>
<html lang="fr">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/svg+xml" href="/vite.svg" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Chronologie de l'informatique</title>
  </head>
  <body>
    <div class="reveal">
      <div class="slides">
    
"""
FOOT = """</div></div>
    <script type="module" src="/main.js"></script>
  </body>
</html>
"""
SLIDE = """<div class="slides">
        <section>Horizontal Slide</section>
        <section>
          <section>Vertical Slide 1</section>
          <section>Vertical Slide 2</section>
        </section>
      </div>"""


def generate_web(input_cards: list, output=HTML_FILE):
    """Génère la page web à partir des données"""
    print(f"Generating {HTML_FILE}")
    html = HEAD
    # create a verical slideshow for each subject
    # type, year, title, picture, description, credits, ascii
    theme = input_cards[0]["type"]
    html += f'<section class="{theme}" data-background="#dddddd">\n<section><h2>{theme_dict[theme]}</h2></section>\n'
    for card in input_cards:
        if theme != card["type"]:
            theme = card["type"]
            html += f"""</section>
            <section class="{theme}">
              <section>
              <h2>{theme_dict[theme]}</h2>
              </section>"""
        html += f'<section><h3>{card["year"]}: {card["title"]}</h3>\n'
        html += f"""<div class="flex">
        <div class="left">{card["description"]}</div>
        <div class="right rot-caption">
          <figure class="figure">
            <img src="/img/{slugify(card["title"])}.jpg" class="figure-img img-fluid rounded" alt="{card["title"]}"/>
          <figcaption class="small">{card["credits"]}</figcaption>
          </figure></div>
        </div>
        </section>"""
    html += "</section>\n"
    html += FOOT

    with open(output, "w") as f:
        f.write(html)
