import os
import pandas as pd
import argparse
from slugify import slugify

from _image import resizer, download
from _website import generate_web

# Configuration

special_path = "./special_cards/"
latex_path = "./latex/"
IMG_PATH = "./www/public/img/"
output_path = "./deck/"
latex_nine_cards_by_page_name = "./cartes_timeline.tex"
rules_front = f"{special_path}0_rules_front.tex"
rules_back = f"{special_path}0_rules_back.tex"
blank_card = f"{special_path}00_blank.tex"


# Initialize argparse.ArgumentParser
parser = argparse.ArgumentParser()

# Command line program
main = "build.py"
description = "Generate tex files for Timeline cards from a csv file"

parser.add_argument(
    "csv_file",
    nargs="?",
    help="The csv input file",
    default="content/timeline.csv",
)
parser.add_argument(
    "--type",
    choices=["timeline", "escape"],
    nargs="?",
    help="""Type of cards game to generate.
    timeline: timeline cards
    escape: escape game where cards conatain ascii values to decode a message""",
    default="timeline",
)
args = parser.parse_args()


# Helpers
def download_picture(card_obj):
    filename = pict_filename(card_obj, "_web_original")
    if not os.path.exists(filename):
        download(card_obj["picture"], filename)
        print(f"Downloaded ({filename})")


def pict_filename(card_obj, suffix=""):
    # TODO Not optimal because extension is hard-coded
    # but we need jpg for latex
    return f'{IMG_PATH}{slugify(card_obj["title"])}{suffix}.jpg'


def card_type(card_obj):
    return f'\\cardtype{card_obj["type"].capitalize()}'


def front_filename(card_obj):
    if card_obj == "blanck":
        return "special_cards/00_blank.tex"
    elif card_obj == "rules":
        if args.type == "timeline":
            return "special_cards/0_rules_front.tex"
        elif args.type == "escape":
            return "special_cards/0_rules_front_escape.tex"

    return f'{output_path}{card_obj["index"]}_{slugify(card_obj["title"])}_front.tex'


def back_filename(card_obj):
    if card_obj == "blanck":
        return "special_cards/00_blank.tex"
    elif card_obj == "rules":
        if args.type == "timeline":
            return "special_cards/0_rules_back.tex"
        elif args.type == "escape":
            return "special_cards/0_rules_back_escape.tex"
    return f'{output_path}{card_obj["index"]}_{slugify(card_obj["title"])}_back.tex'


# Main content of .tex files associated with a card
def front_content(card_obj, credits_color):
    title = card_obj["title"]
    assert (
        len(title) < 69
    ), f"Title of {card_obj['title']} is too long, {len(title)} >= 69"
    credits = card_obj["credits"]
    assert (
        len(credits) < 100
    ), f"Credits of {card_obj['title']} is too long, {len(credits)} >= 100"
    return f"""
\\begin{{tikzpicture}}
    \\cardborder
    \\cardfrontbackground{{{pict_filename(card_obj)}}}
    {card_type(card_obj)}
    \\cardtitle{{{title}}}
    \\cardcredits[{credits_color}]{{{credits}}}
\\end{{tikzpicture}}
"""


def back_content(card_obj):
    N = len(card_obj["description"])
    assert N < 505, f"Description of {card_obj['title']} is too long, {N} >= 505"
    return f"""
\\begin{{tikzpicture}}
    \\cardborder
    \\cardbackbackground{{{pict_filename(card_obj)}}}
    {card_type(card_obj)}
    \\cardcontent{{{card_obj["year"]}}}{{{card_obj["description"]}}}
\\end{{tikzpicture}}
"""


# Main PDF with nine cards per page
def generate_latex_nine_cards_by_page(cards):
    print("Generating 3x3 plates")

    # adapt card list to have a multiple of 9 cards with rules and blank cards
    N = len(cards)

    document = ""
    for i in range(0, 9 * (N + 1) // 9, 9):
        document += "  \\begin{center}\n    \\begin{tabular}{@{}c@{\\hspace{2mm}}c@{\\hspace{2mm}}c@{}}%\n"
        for j in range(3):
            for k in range(3):
                idx = i + j * 3 + k
                if idx == N:
                    document += f"      \\input{{{front_filename('rules')}}}"
                elif idx > N:
                    document += f"      \\input{{{front_filename('blanck')}}}"
                else:
                    document += f"      \\input{{{front_filename(cards[idx])}}}"
                if k < 2:
                    document += "&%\n"
                else:
                    document += "\\\\%\n"

        document += "    \\end{tabular}\n    \\end{center}\n    \\clearpage%\n"

        document += "  \\begin{center}\n    \\begin{tabular}{@{}c@{\\hspace{2mm}}c@{\\hspace{2mm}}c@{}}%\n"
        for j in range(3):
            for k in range(2, -1, -1):
                idx = i + j * 3 + k
                if idx == N:
                    document += f"      \\input{{{back_filename('rules')}}}"
                elif idx > N:
                    document += f"      \\input{{{back_filename('blanck')}}}"
                else:
                    document += f"      \\input{{{back_filename(cards[idx])}}}"
                if k > 0:
                    document += "&%\n"
                else:
                    document += "\\\\%\n"

        document += "    \\end{tabular}\n    \\end{center}\n    \\clearpage%\n"

    latex_content = f"""
\\documentclass[a4paper]{{article}}

\\input{{./{latex_path}/packages}}
\\input{{./{latex_path}/tikzcards}}

\\geometry{{hmargin=5mm, vmargin=10mm}}

\\begin{{document}}%
{document}
\\end{{document}}
"""

    with open(latex_nine_cards_by_page_name, "w") as f:
        f.write(latex_content)


# Main program loop
def generate_tex(cards, type=None):
    print(f"Generating timeline with {len(cards)} cards")
    if type is not None:
        cards = [card for card in cards if card["type"] == type]
    for card in cards:
        download_picture(card)
        credits_color = resizer(
            pict_filename(card, "_web_original"), pict_filename(card)
        )

        with open(front_filename(card), "w") as f:
            f.write(front_content(card, credits_color))
        with open(back_filename(card), "w") as f:
            f.write(back_content(card))

    generate_latex_nine_cards_by_page(cards)


input_cards = []

if not os.path.exists(IMG_PATH):
    os.makedirs(IMG_PATH)
if not os.path.exists(output_path):
    os.makedirs(output_path)

# read csv as list of dicts
input_cards = pd.read_csv(args.csv_file).reset_index().to_dict("records")
print(f"Read {len(input_cards)} cards from {args.csv_file}")


if args.type == "escape":
    print("ESCAPE GAME MODE: Using ascii column value as year")
    for card in input_cards:
        ascii_code = str(card["ascii"])
        assert (
            int(ascii_code, 2) < 128
        ), f"{ascii_code} of {card['title']} is not a valid ascii code"
        card["year"] = ascii_code.zfill(7)

# timeline(input_cards, "soft")
generate_tex(input_cards)
generate_web(input_cards)
