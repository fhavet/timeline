import { resolve } from 'path'
import { defineConfig } from "vite";

export default defineConfig({
  assetsInclude: ["**/*.pdf"],
  base: "/jeux/timeline/",
  build: {
    outDir: "../public",
    rollupOptions: {
      input: {
        main: resolve(__dirname, "index.html"),
        diapo: resolve(__dirname, "timeline.html"),
      },
    },
  },
});
